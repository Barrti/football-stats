package pl.bmiedlar.stats.match;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Bartosz on 15.08.2015.
 */
@Service
public class RefereeService {

    @Autowired
    RefereeRepository refereeRepository;

    public Referee saveReferee(Referee referee) {
        return refereeRepository.save(referee);
    }

    public List<Referee> findAllReferees() {
        return refereeRepository.findAll();
    }

    public Referee findRefereeById(int id) {
        return refereeRepository.findOne(id);
    }

    public Referee updateReferee(int id, Referee referee) {
        Referee ref = referee;
        ref.setId(id);
        return refereeRepository.save(ref);
    }
}
