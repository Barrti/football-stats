package pl.bmiedlar.stats.match;

import pl.bmiedlar.stats.info.PlayerDTO;
import pl.bmiedlar.stats.info.Team;

import java.util.List;

/**
 * Created by Bartosz on 15.08.2015.
 */
public class TeamLineup {

    private Team team;
    private List<PlayerDTO> starting;
    private List<PlayerDTO> bench;

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<PlayerDTO> getStarting() {
        return starting;
    }

    public void setStarting(List<PlayerDTO> starting) {
        this.starting = starting;
    }

    public List<PlayerDTO> getBench() {
        return bench;
    }

    public void setBench(List<PlayerDTO> bench) {
        this.bench = bench;
    }
}
