package pl.bmiedlar.stats.match;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bmiedlar.stats.events.ScoreService;
import pl.bmiedlar.stats.info.MatchdayRepository;
import pl.bmiedlar.stats.info.TeamRepository;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Service
public class MatchService {

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    MatchdayRepository matchdayRepository;

    @Autowired
    RefereeRepository refereeRepository;

    @Autowired
    LineupRepository lineupRepository;

    @Autowired
    LineupWrapper lineupWrapper;

    @Autowired
    ScoreService scoreService;

    public Match saveMatch(MatchDTO dto) {
        Match match = new Match();

        match.setAwayTeam(teamRepository.findOne(dto.getIdAwayTeam()));
        match.setHomeTeam(teamRepository.findOne(dto.getIdHomeTeam()));
        match.setMatchday(matchdayRepository.findOne(dto.getIdMatchday()));
        match.setReferee(refereeRepository.findOne(dto.getIdReferee()));
        match.setMatchDate(dto.getMatchDate());
        match.setSquads(false);

        Match saved = matchRepository.save(match);

        scoreService.createScore(saved.getId());

        return saved;
    }

    public Set<Match> findAllMatches() {
        return new LinkedHashSet<>(matchRepository.findAll());
    }

    public Match findOneMatch(int id) {
        return matchRepository.findOne(id);
    }

    public Match updateMatch(Match match) {
        return matchRepository.save(match);
    }

    public void startMatch(int id) {
        Match match = matchRepository.findOne(id);
        if (match.getStartDate() == null) {
            match.setStartDate(new Date(System.currentTimeMillis()));
            matchRepository.save(match);
        }

    }

    public void stopFirstHalf(int id) {
        Match match = matchRepository.findOne(id);
        if(match.getStartDate() != null && match.getHalfEndDate() == null) {
            match.setHalfEndDate(new Date(System.currentTimeMillis()));
            matchRepository.save(match);
        }
    }

    public void startSecondHalf(int id) {
        Match match = matchRepository.findOne(id);
        if(match.getStartDate() != null && match.getHalfEndDate() != null && match.getSecondHalfStart() == null) {
            match.setSecondHalfStart(new Date(System.currentTimeMillis()));
            matchRepository.save(match);
        }
    }

    public void finishMatch(int id) {
        Match match = matchRepository.findOne(id);
        if(match.getStartDate() != null && match.getHalfEndDate() != null && match.getSecondHalfStart() != null && match.getSecondHalfEnd() == null) {
            match.setSecondHalfEnd(new Date(System.currentTimeMillis()));
            matchRepository.save(match);
        }
    }

    public MatchLineupDTO findOneMatchById(int id) {
        MatchLineupDTO matchLineupDTO = new MatchLineupDTO();
        Match match = matchRepository.findOne(id);

        matchLineupDTO.setId(match.getId());
        List<Lineup> lineups = lineupRepository.findByMatchId(id);
        lineups.forEach(lineup -> {
            if (lineup.getTeam().getId() == match.getHomeTeam().getId())
                matchLineupDTO.setHomeTeam(lineupWrapper.wrapToTeamLineup(lineup.getId()));
            else if (lineup.getTeam().getId() == match.getAwayTeam().getId())
                matchLineupDTO.setAwayTeam(lineupWrapper.wrapToTeamLineup(lineup.getId()));
        });
        matchLineupDTO.setMatchday(match.getMatchday().getName());
        matchLineupDTO.setMatchDate(match.getMatchDate());
        matchLineupDTO.setReferee(match.getReferee().getName());

        return matchLineupDTO;
    }

    public List<Match> findByMatchday(int id) {
        return matchRepository.findByMatchdayId(id);
    }
}
