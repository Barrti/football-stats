package pl.bmiedlar.stats.match;

import java.util.List;

/**
 * Created by Bartosz on 15.08.2015.
 */
public class LineupAddDTO {

    private int id;
    private int idTeam;
    private int idMatch;
    private List<Integer> startingEleven;
    private List<Integer> bench;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(int idTeam) {
        this.idTeam = idTeam;
    }

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public List<Integer> getStartingEleven() {
        return startingEleven;
    }

    public void setStartingEleven(List<Integer> startingEleven) {
        this.startingEleven = startingEleven;
    }

    public List<Integer> getBench() {
        return bench;
    }

    public void setBench(List<Integer> bench) {
        this.bench = bench;
    }
}
