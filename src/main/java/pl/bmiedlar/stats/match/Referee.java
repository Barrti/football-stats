package pl.bmiedlar.stats.match;

import javax.persistence.*;

/**
 * Created by Bartosz on 15.08.2015.
 */
@Entity
@Table
public class Referee {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column
    private int id;

    @Column
    private String name;

    @Column
    private String region;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
