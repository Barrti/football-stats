package pl.bmiedlar.stats.match;

import pl.bmiedlar.stats.info.PlayerDTO;

import java.util.List;

/**
 * Created by Bartosz on 14.08.2015.
 */
public class LineupDTO {

    private int id;
    private int idTeam;
    private int idMatch;
    private List<PlayerDTO> startingEleven;
    private List<PlayerDTO> bench;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(int idTeam) {
        this.idTeam = idTeam;
    }

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public List<PlayerDTO> getStartingEleven() {
        return startingEleven;
    }

    public void setStartingEleven(List<PlayerDTO> startingEleven) {
        this.startingEleven = startingEleven;
    }

    public List<PlayerDTO> getBench() {
        return bench;
    }

    public void setBench(List<PlayerDTO> bench) {
        this.bench = bench;
    }
}
