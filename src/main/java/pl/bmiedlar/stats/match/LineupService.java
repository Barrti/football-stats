package pl.bmiedlar.stats.match;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Service
public class LineupService {

    @Autowired
    LineupWrapper lineupWrapper;
    @Autowired
    LineupRepository lineupRepository;
    @Autowired
    MatchRepository matchRepository;
    @Autowired
    MatchService matchService;

    public LineupDTO saveLineup(LineupAddDTO dto) {
        LineupDTO ldto = lineupWrapper.wrapToDTO(lineupRepository.save(lineupWrapper.wrapToEntity(dto)));
        Match match = matchRepository.findOne(ldto.getIdMatch());
        match.setSquads(true);
        matchRepository.save(match);
        return ldto;
    }

    public List<LineupDTO> getAllLineups() {
        List<Lineup> lineups = lineupRepository.findAll();
        List<LineupDTO> dtos = new ArrayList<>();

        lineups.forEach(lineup -> dtos.add(lineupWrapper.wrapToDTO(lineup)));
        return dtos;
    }

    public LineupDTO findLineupById(int id) {
        return lineupWrapper.wrapToDTO(lineupRepository.findOne(id));
    }

    public LineupDTO updateLineup(int id, LineupDTO dto) {
        Lineup lineup = lineupWrapper.wrapToEntity(dto);
        lineup.setId(id);

        return lineupWrapper.wrapToDTO(lineupRepository.save(lineup));
    }

    public List<LineupDTO> findLineupsByMatch(int id) {
        List<Lineup> lineups = lineupRepository.findByMatchId(id);
        List<LineupDTO> dtos = new ArrayList<>();

        lineups.forEach(lineup -> dtos.add(lineupWrapper.wrapToDTO(lineup)));
        return dtos;
    }
}
