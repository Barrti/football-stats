package pl.bmiedlar.stats.match;

import java.util.Date;

/**
 * Created by Bartosz on 14.08.2015.
 */
public class MatchDTO {

    private int id;
    private String matchDate;
    private Date halfEndDate;
    private Date secondHalfStart;
    private Date secondHalfEnd;
    private long firstHalfDuration;
    private long secondHalfDuration;
    private int idHomeTeam;
    private int idAwayTeam;
    private int idMatchday;
    private int idReferee;
    private boolean squads;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public Date getHalfEndDate() {
        return halfEndDate;
    }

    public void setHalfEndDate(Date halfEndDate) {
        this.halfEndDate = halfEndDate;
    }

    public Date getSecondHalfStart() {
        return secondHalfStart;
    }

    public void setSecondHalfStart(Date secondHalfStart) {
        this.secondHalfStart = secondHalfStart;
    }

    public Date getSecondHalfEnd() {
        return secondHalfEnd;
    }

    public void setSecondHalfEnd(Date secondHalfEnd) {
        this.secondHalfEnd = secondHalfEnd;
    }

    public long getFirstHalfDuration() {
        return firstHalfDuration;
    }

    public void setFirstHalfDuration(long firstHalfDuration) {
        this.firstHalfDuration = firstHalfDuration;
    }

    public long getSecondHalfDuration() {
        return secondHalfDuration;
    }

    public void setSecondHalfDuration(long secondHalfDuration) {
        this.secondHalfDuration = secondHalfDuration;
    }

    public int getIdHomeTeam() {
        return idHomeTeam;
    }

    public void setIdHomeTeam(int idHomeTeam) {
        this.idHomeTeam = idHomeTeam;
    }

    public int getIdAwayTeam() {
        return idAwayTeam;
    }

    public void setIdAwayTeam(int idAwayTeam) {
        this.idAwayTeam = idAwayTeam;
    }

    public int getIdMatchday() {
        return idMatchday;
    }

    public void setIdMatchday(int idMatchday) {
        this.idMatchday = idMatchday;
    }

    public int getIdReferee() {
        return idReferee;
    }

    public void setIdReferee(int idReferee) {
        this.idReferee = idReferee;
    }

    public boolean hasSquads() {
        return squads;
    }

    public void setSquads(boolean squads) {
        this.squads = squads;
    }
}
