package pl.bmiedlar.stats.match;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by bmiedlar on 2015-08-13.
 */
public interface MatchRepository extends JpaRepository<Match, Integer> {

    List<Match> findByMatchdayId(int id);
}
