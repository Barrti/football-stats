package pl.bmiedlar.stats.match;


/**
 * Created by Bartosz on 15.08.2015.
 */
public class MatchLineupDTO {

    private int id;
    private String referee;
    private String matchDate;
    private TeamLineup homeTeam;
    private TeamLineup awayTeam;
    private String matchday;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReferee() {
        return referee;
    }

    public void setReferee(String referee) {
        this.referee = referee;
    }

    public TeamLineup getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(TeamLineup homeTeam) {
        this.homeTeam = homeTeam;
    }

    public TeamLineup getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(TeamLineup awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public String getMatchday() {
        return matchday;
    }

    public void setMatchday(String matchday) {
        this.matchday = matchday;
    }
}
