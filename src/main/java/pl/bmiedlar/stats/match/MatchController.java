package pl.bmiedlar.stats.match;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import pl.bmiedlar.stats.output.*;

import java.util.List;
import java.util.Set;

/**
 * Created by Bartosz on 14.08.2015.
 */
@RestController
public class MatchController {

    @Autowired
    MatchService matchService;

    @Autowired
    LineupService lineupService;

    @Autowired
    RefereeService refereeService;

    @Autowired
    MatchOutputWrapper matchOutputWrapper;

    @Autowired
    TimeService timeService;

    @RequestMapping(value = "/api/out/matches", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<MatchOutputList> findAllMOL() {
        return matchOutputWrapper.getAllMatches();
    }

    @RequestMapping(value = "/api/out/matches/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public MatchOutput oneMatchOut(@PathVariable(value = "id") int id) {
        return matchOutputWrapper.createMatchOutput(id);
    }

    @RequestMapping(value = "/api/out/matches/{id}/times", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public TimeOnPitch timesOut(@PathVariable(value = "id") int id) {
        return timeService.createToP(id);
    }

    @RequestMapping(value = "/api/matches", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<Match> findAllMatches() {
        return matchService.findAllMatches();
    }

    @RequestMapping(value = "/api/matches/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Match findOneMatch(@PathVariable(value = "id") int id) {
        return matchService.findOneMatch(id);
    }

    @RequestMapping(value = "/api/matches/info/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public MatchLineupDTO findML(@PathVariable(value = "id") int id) {
        return matchService.findOneMatchById(id);
    }

    @RequestMapping(value = "/api/matches", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Match saveMatch(@RequestBody MatchDTO dto) {
        return matchService.saveMatch(dto);
    }

    @RequestMapping(value = "/api/matches/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public Match updateMatch(@RequestBody Match match, @PathVariable(value = "id") int id) {
        return matchService.updateMatch(match);
    }

    @RequestMapping(value = "/api/matches/{id}/firsthalf/start", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void startFirstHalf(@PathVariable(value = "id") int id) {
        matchService.startMatch(id);
    }

    @RequestMapping(value = "/api/matches/{id}/firsthalf/finish", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void finishFirstHalf(@PathVariable(value = "id") int id) {
        matchService.stopFirstHalf(id);
    }

    @RequestMapping(value = "/api/matches/{id}/secondhalf/start", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void startSecondHalf(@PathVariable(value = "id") int id) {
        matchService.startSecondHalf(id);
    }

    @RequestMapping(value = "/api/matches/{id}/secondhalf/finish", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void finishSecondHalf(@PathVariable(value = "id") int id) {
        matchService.finishMatch(id);
    }

    @RequestMapping(value = "/api/matchdays/{id}/matches", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Match> findByMatchday(@PathVariable(value = "id") int id) {
        return matchService.findByMatchday(id);
    }

    // ---------------------

    @RequestMapping(value = "/api/referees", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<Referee> findAllReferees(){
        return refereeService.findAllReferees();
    }

    @RequestMapping(value = "/api/referees", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Referee saveReferee(@RequestBody Referee referee) {
        return refereeService.saveReferee(referee);
    }

    @RequestMapping(value = "/api/referees/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Referee findRefereeById(@PathVariable(value = "id") int id) {
        return refereeService.findRefereeById(id);
    }

    // ---------------------

    @RequestMapping(value = "/api/lineups", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<LineupDTO> findAllLineups() {
        return lineupService.getAllLineups();
    }

    @RequestMapping(value = "/api/lineups", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public LineupDTO saveLineup(@RequestBody LineupAddDTO dto) {
        return lineupService.saveLineup(dto);
    }

    @RequestMapping(value = "/api/lineups/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public LineupDTO getOneLineup(@PathVariable(value = "id") int id) {
        return lineupService.findLineupById(id);
    }

    @RequestMapping(value = "/api/lineups/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public LineupDTO updateLineup(@PathVariable(value = "id") int id, @RequestBody LineupDTO dto) {
        return lineupService.updateLineup(id, dto);
    }

    @RequestMapping(value = "/api/matches/{id}/lineups", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<LineupDTO> findLineupsByMatch(@PathVariable(value = "id") int id) {
        return lineupService.findLineupsByMatch(id);
    }
}
