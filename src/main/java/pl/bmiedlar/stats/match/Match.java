package pl.bmiedlar.stats.match;

import pl.bmiedlar.stats.info.Matchday;
import pl.bmiedlar.stats.info.Team;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by bmiedlar on 2015-08-12.
 */

@Entity
@Table
public class Match {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column
    private int id;

    @ManyToOne
    private Matchday matchday;

    @Column
    private String matchDate;

    @Transient
    private Date startDate;

    @Transient
    private Date halfEndDate;

    @Transient
    private Date secondHalfStart;

    @Transient
    private Date secondHalfEnd;

    @ManyToOne
    private Team homeTeam;

    @ManyToOne
    private Team awayTeam;

    @ManyToOne
    private Referee referee;

    @Column
    private boolean squads;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Matchday getMatchday() {
        return matchday;
    }

    public void setMatchday(Matchday matchday) {
        this.matchday = matchday;
    }

    public String getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(String matchDate) {
        this.matchDate = matchDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getHalfEndDate() {
        return halfEndDate;
    }

    public void setHalfEndDate(Date halfEndDate) {
        this.halfEndDate = halfEndDate;
    }

    public Date getSecondHalfStart() {
        return secondHalfStart;
    }

    public void setSecondHalfStart(Date secondHalfStart) {
        this.secondHalfStart = secondHalfStart;
    }

    public Date getSecondHalfEnd() {
        return secondHalfEnd;
    }

    public void setSecondHalfEnd(Date secondHalfEnd) {
        this.secondHalfEnd = secondHalfEnd;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(Team homeTeam) {
        this.homeTeam = homeTeam;
    }

    public Team getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(Team awayTeam) {
        this.awayTeam = awayTeam;
    }

    public Referee getReferee() {
        return referee;
    }

    public void setReferee(Referee referee) {
        this.referee = referee;
    }

    public boolean isSquads() {
        return squads;
    }

    public void setSquads(boolean squads) {
        this.squads = squads;
    }
}
