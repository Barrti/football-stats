package pl.bmiedlar.stats.match;

import pl.bmiedlar.stats.info.Player;
import pl.bmiedlar.stats.info.Team;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Entity
@Table
public class Lineup {

    @Id @GeneratedValue(strategy = GenerationType.TABLE)
    @Column
    private int id;

    @ManyToOne
    private Team team;

    @ManyToOne
    private Match match;

    @ManyToMany
    private List<Player> startingEleven;

    @ManyToMany
    private List<Player> bench;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public List<Player> getStartingEleven() {
        return startingEleven;
    }

    public void setStartingEleven(List<Player> startingEleven) {
        this.startingEleven = startingEleven;
    }

    public List<Player> getBench() {
        return bench;
    }

    public void setBench(List<Player> bench) {
        this.bench = bench;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
