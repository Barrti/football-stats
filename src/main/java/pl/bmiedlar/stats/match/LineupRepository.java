package pl.bmiedlar.stats.match;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Bartosz on 14.08.2015.
 */
public interface LineupRepository extends JpaRepository<Lineup, Integer> {

    List<Lineup> findByMatchId(int id);
}
