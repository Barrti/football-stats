package pl.bmiedlar.stats.match;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bartosz on 15.08.2015.
 */
public interface RefereeRepository extends JpaRepository<Referee, Integer> {
}
