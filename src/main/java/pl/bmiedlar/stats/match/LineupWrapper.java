package pl.bmiedlar.stats.match;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bmiedlar.stats.info.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Component
public class LineupWrapper {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    PlayerWrapper playerWrapper;

    @Autowired
    LineupRepository lineupRepository;

    public Lineup wrapToEntity(LineupDTO dto) {
        Lineup lineup = new Lineup();

        lineup.setMatch(matchRepository.findOne(dto.getIdMatch()));
        List<Player> bench = new ArrayList<>();
        dto.getBench().forEach(player -> bench.add(playerRepository.findOne(player.getId())));
        List<Player> starting = new ArrayList<>();
        dto.getStartingEleven().forEach(player -> starting.add(playerRepository.findOne(player.getId())));
        lineup.setBench(bench);
        lineup.setStartingEleven(starting);
        lineup.setTeam(teamRepository.findOne(dto.getIdTeam()));

        return lineup;
    }

    public Lineup wrapToEntity(LineupAddDTO dto) {
        Lineup lineup = new Lineup();

        lineup.setMatch(matchRepository.findOne(dto.getIdMatch()));
        List<Player> bench = new ArrayList<>();
        dto.getBench().forEach(player -> bench.add(playerRepository.findOne(player)));
        List<Player> starting = new ArrayList<>();
        dto.getStartingEleven().forEach(player -> starting.add(playerRepository.findOne(player)));
        lineup.setBench(bench);
        lineup.setStartingEleven(starting);
        lineup.setTeam(teamRepository.findOne(dto.getIdTeam()));

        return lineup;
    }

    public LineupDTO wrapToDTO(Lineup lineup) {
        LineupDTO dto = new LineupDTO();

        dto.setId(lineup.getId());
        dto.setIdMatch(lineup.getMatch().getId());
        dto.setIdTeam(lineup.getTeam().getId());
        List<PlayerDTO> bench = new ArrayList<>();
        lineup.getBench().forEach(player -> bench.add(playerWrapper.wrapToDTO(playerRepository.findOne(player.getId()))));
        List<PlayerDTO> starting = new ArrayList<>();
        lineup.getStartingEleven().forEach(player -> starting.add(playerWrapper.wrapToDTO(playerRepository.findOne(player.getId()))));
        dto.setBench(bench);
        dto.setStartingEleven(starting);

        return dto;
    }

    public TeamLineup wrapToTeamLineup(int id) {
        Lineup lineup = lineupRepository.findOne(id);
        TeamLineup teamLineup = new TeamLineup();

        teamLineup.setTeam(lineup.getTeam());
        List<PlayerDTO> bench = new ArrayList<>();
        lineup.getBench().forEach(player -> bench.add(playerWrapper.wrapToDTO(playerRepository.findOne(player.getId()))));
        List<PlayerDTO> starting = new ArrayList<>();
        lineup.getStartingEleven().forEach(player -> starting.add(playerWrapper.wrapToDTO(playerRepository.findOne(player.getId()))));
        teamLineup.setStarting(starting);
        teamLineup.setBench(bench);

        return teamLineup;
    }

}
