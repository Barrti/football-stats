package pl.bmiedlar.stats.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by Bartosz on 08.08.2015.
 */
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public User createUser(UserDTO dto) {
        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPassword(new BCryptPasswordEncoder().encode(dto.getPassword()));

        return userRepository.save(user);
    }
}
