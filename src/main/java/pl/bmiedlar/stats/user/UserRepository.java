package pl.bmiedlar.stats.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bartosz on 08.08.2015.
 */
public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
