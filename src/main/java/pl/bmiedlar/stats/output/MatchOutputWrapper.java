package pl.bmiedlar.stats.output;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bmiedlar.stats.events.*;
import pl.bmiedlar.stats.info.TeamWrapper;
import pl.bmiedlar.stats.match.Match;
import pl.bmiedlar.stats.match.MatchRepository;
import pl.bmiedlar.stats.match.MatchService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by bmiedlar on 2015-08-17.
 */
@Component
public class MatchOutputWrapper {

    @Autowired
    MatchRepository matchRepository;

    @Autowired
    PenaltyRepositroy penaltyRepositroy;

    @Autowired
    CardRepository cardRepository;

    @Autowired
    GoalRepository goalRepository;

    @Autowired
    AssistRepository assistRepository;

    @Autowired
    SubstituteRepository substituteRepository;

    @Autowired
    MatchService matchService;

    @Autowired
    TeamWrapper teamWrapper;

    @Autowired
    ScoreRepository scoreRepository;

    @Autowired
    TimeService timeService;

    public MatchOutput createMatchOutput(int idMatch) {
        MatchOutput matchOutput = new MatchOutput();

        matchOutput.setMatchId(idMatch);
        matchOutput.setCardsInfo(createCardInfo(idMatch));
        matchOutput.setGoalsInfo(createGoalInfo(idMatch));
        matchOutput.setAssistsInfo(createAssistInfo(idMatch));
        matchOutput.setSubstitutesInfo(createSubstituteInfo(idMatch));
        matchOutput.setMatchInfo(matchService.findOneMatchById(idMatch));
        matchOutput.setScoreBoard(createScoreBoard(idMatch));
        matchOutput.setPenaltiesInfo(createPenaltyInfo(idMatch));
        matchOutput.setTimeOnPitch(timeService.createToP(idMatch));

        return matchOutput;
    }

    private PenaltiesInfo createPenaltyInfo(int idMatch) {
        PenaltiesInfo penaltiesInfo = new PenaltiesInfo();
        Map<Integer, List<PenaltyInfo>> info = new HashMap<>();
        List<PenaltyInfo> homeTeamPenalties = new ArrayList<>();
        List<PenaltyInfo> awayTeamPenalties = new ArrayList<>();
        List<Penalty> penalties = new ArrayList<>(penaltyRepositroy.findByMatchId(idMatch));
        Match match = matchRepository.findOne(idMatch);

        penalties.forEach(penalty -> {
            PenaltyInfo pi = new PenaltyInfo();
            pi.setCause(penalty.getCause());
            pi.setPenaltyFor(teamWrapper.wrapToDTO(penalty.getTeam()));
            pi.setPlayerExecutorId(penalty.getExecutor().getId());
            pi.setPlayerExecutor(penalty.getExecutor().getFirstName() + " " + penalty.getExecutor().getLastName());
            pi.setPlayerFouledId(penalty.getFouled().getId());
            pi.setPlayerFouled(penalty.getFouled().getFirstName() + " " + penalty.getFouled().getLastName());
            pi.setPlayerFoulerId(penalty.getFouler().getId());
            pi.setPlayerFouler(penalty.getFouler().getFirstName() + " " + penalty.getFouler().getLastName());
            pi.setTime(penalty.getTime());
            pi.setType(penalty.getType());
            if(match.getHomeTeam().getId() == pi.getPenaltyFor().getId())
                homeTeamPenalties.add(pi);
            else
                awayTeamPenalties.add(pi);
        });

        info.put(match.getHomeTeam().getId(), homeTeamPenalties);
        info.put(match.getAwayTeam().getId(), awayTeamPenalties);

        penaltiesInfo.setIdMatch(idMatch);
        penaltiesInfo.setPenaltiesInfo(info);

        return penaltiesInfo;
    }

    private CardsInfo createCardInfo(int idMatch) {
        List<CardInfo> homeTeamCards = new ArrayList<>();
        List<CardInfo> awayTeamCards = new ArrayList<>();
        List<Card> cards = new ArrayList<>(cardRepository.findByMatchId(idMatch));
        Map<Integer, List<CardInfo>> cardsInfo = new HashMap<>();
        CardsInfo info = new CardsInfo();
        Match match = matchRepository.findOne(idMatch);

        cards.forEach(card -> {
            CardInfo cardConsumer = new CardInfo();
            cardConsumer.setType(card.getType());
            cardConsumer.setTime(card.getTime());
            cardConsumer.setPlayerId(card.getPlayer().getId());
            cardConsumer.setPlayer(card.getPlayer().getFirstName() + " " + card.getPlayer().getLastName());
            cardConsumer.setCardFor(teamWrapper.wrapToDTO(card.getTeam()));
            if(card.getTeam().getId() == match.getHomeTeam().getId())
                homeTeamCards.add(cardConsumer);
            else
                awayTeamCards.add(cardConsumer);
        });

        cardsInfo.put(match.getHomeTeam().getId(), homeTeamCards);
        cardsInfo.put(match.getAwayTeam().getId(), awayTeamCards);

        info.setIdMatch(idMatch);
        info.setCardInfo(cardsInfo);

        return info;
    }

    private GoalsInfo createGoalInfo(int idMatch) {
        List<GoalInfo> homeTeamGoals = new ArrayList<>();
        List<GoalInfo> awayTeamGoals = new ArrayList<>();
        List<Goal> goals = new ArrayList<>(goalRepository.findByMatchId(idMatch));
        Map<Integer, List<GoalInfo>> goalsInfo = new HashMap<>();
        GoalsInfo info = new GoalsInfo();
        Match match = matchRepository.findOne(idMatch);

        goals.forEach(goal -> {
            GoalInfo gi = new GoalInfo();
            gi.setType(goal.getType());
            gi.setTime(goal.getTime());
            gi.setPlayerId(goal.getPlayer().getId());
            gi.setPlayer(goal.getPlayer().getFirstName() + " " + goal.getPlayer().getLastName());
            gi.setGoalFor(teamWrapper.wrapToDTO(goal.getTeam()));
            if(goal.getTeam().getId() == match.getHomeTeam().getId())
                homeTeamGoals.add(gi);
            else
                awayTeamGoals.add(gi);
        });

        goalsInfo.put(match.getHomeTeam().getId(), homeTeamGoals);
        goalsInfo.put(match.getAwayTeam().getId(), awayTeamGoals);

        info.setIdMatch(idMatch);
        info.setGoalInfo(goalsInfo);

        return info;
    }

    private ScoreBoard createScoreBoard(int idMatch) {
        ScoreBoard scoreBoard = new ScoreBoard();
        Match match = matchRepository.findOne(idMatch);
        Score score = scoreRepository.findByMatchId(idMatch);

        scoreBoard.setAwayTeamId(match.getAwayTeam().getId());
        scoreBoard.setAwayTeamScore(score.getAwayScore());
        scoreBoard.setHomeTeamId(match.getHomeTeam().getId());
        scoreBoard.setHomeTeamScore(score.getHomeScore());

        return scoreBoard;
    }

    private AssistsInfo createAssistInfo(int idMatch) {
        List<AssistInfo> homeTeamAssists = new ArrayList<>();
        List<AssistInfo> awayTeamAssists = new ArrayList<>();
        List<Assist> assists = new ArrayList<>(assistRepository.findByMatchId(idMatch));
        Map<Integer, List<AssistInfo>> assistsInfo = new HashMap<>();
        AssistsInfo info = new AssistsInfo();
        Match match = matchRepository.findOne(idMatch);

        assists.forEach(assist -> {
            AssistInfo assistConsumer = new AssistInfo();
            assistConsumer.setType(assist.getType());
            assistConsumer.setTime(assist.getTime());
            assistConsumer.setPlayerId(assist.getPlayer().getId());
            assistConsumer.setPlayerName(assist.getPlayer().getFirstName() + " " + assist.getPlayer().getLastName());
            assistConsumer.setAssistFor(teamWrapper.wrapToDTO(assist.getTeam()));
            if(assist.getTeam().getId() == match.getHomeTeam().getId())
                homeTeamAssists.add(assistConsumer);
            else
                awayTeamAssists.add(assistConsumer);
        });

        assistsInfo.put(match.getHomeTeam().getId(), homeTeamAssists);
        assistsInfo.put(match.getAwayTeam().getId(), awayTeamAssists);

        info.setIdMatch(idMatch);
        info.setAssistInfo(assistsInfo);

        return info;
    }

    private SubstitutesInfo createSubstituteInfo(int idMatch) {
        List<SubstituteInfo> homeTeamSubstitutes = new ArrayList<>();
        List<SubstituteInfo> awayTeamSubstitutes = new ArrayList<>();
        SubstitutesInfo substitutesInfo = new SubstitutesInfo();
        Map<Integer, List<SubstituteInfo>> map = new HashMap<>();
        List<Substitute> substitutes = new ArrayList<>(substituteRepository.findByMatchId(idMatch));
        Match match = matchRepository.findOne(idMatch);

        substitutes.forEach(substitute -> {
            SubstituteInfo si = new SubstituteInfo();
            si.setTime(substitute.getTime());
            si.setPlayerInId(substitute.getPlayerIn().getId());
            si.setPlayerIn(substitute.getPlayerIn().getFirstName() + " " + substitute.getPlayerIn().getLastName());
            si.setPlayerOutId(substitute.getPlayerOut().getId());
            si.setPlayerOut(substitute.getPlayerOut().getFirstName() + " " + substitute.getPlayerOut().getLastName());
            si.setTeam(teamWrapper.wrapToDTO(substitute.getTeam()));
            if(match.getHomeTeam().getId() == substitute.getTeam().getId())
                homeTeamSubstitutes.add(si);
            else
                awayTeamSubstitutes.add(si);
        });

        map.put(match.getHomeTeam().getId(), homeTeamSubstitutes);
        map.put(match.getAwayTeam().getId(), awayTeamSubstitutes);

        substitutesInfo.setMatchId(idMatch);
        substitutesInfo.setSubstitutesInfo(map);

        return substitutesInfo;
    }

    public List<MatchOutputList> getAllMatches() {
        List<Match> matches = matchRepository.findAll();
        List<MatchOutputList> mol = new ArrayList<>();

        matches.forEach(match -> {
            MatchOutputList matchOutputList = new MatchOutputList();
            matchOutputList.setId(match.getId());
            matchOutputList.setMatchday(match.getMatchday().getName());
            matchOutputList.setMatch(match.getHomeTeam().getName() + " - " + match.getAwayTeam().getName());
            matchOutputList.setMatchDate(match.getMatchDate());
            mol.add(matchOutputList);
        });

        return mol;
    }
}
