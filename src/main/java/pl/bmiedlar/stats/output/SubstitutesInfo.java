package pl.bmiedlar.stats.output;

import java.util.List;
import java.util.Map;

/**
 * Created by Bartosz on 07.09.2015.
 */
public class SubstitutesInfo {

    private int matchId;
    private Map<Integer, List<SubstituteInfo>> substitutesInfo;

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public Map<Integer, List<SubstituteInfo>> getSubstitutesInfo() {
        return substitutesInfo;
    }

    public void setSubstitutesInfo(Map<Integer, List<SubstituteInfo>> substitutesInfo) {
        this.substitutesInfo = substitutesInfo;
    }
}
