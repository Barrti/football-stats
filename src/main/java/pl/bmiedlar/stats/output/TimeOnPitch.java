package pl.bmiedlar.stats.output;

import pl.bmiedlar.stats.info.PlayerTime;

import java.util.Map;

/**
 * Created by Bartosz on 28.08.2015.
 */
public class TimeOnPitch {

    private int idMatch;
    private Map<Integer, PlayerTime> playerTimes;

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public Map<Integer, PlayerTime> getPlayerTimes() {
        return playerTimes;
    }

    public void setPlayerTimes(Map<Integer, PlayerTime> playerTimes) {
        this.playerTimes = playerTimes;
    }
}
