package pl.bmiedlar.stats.output;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.bmiedlar.stats.info.TeamDTO;

/**
 * Created by bmiedlar on 2015-08-17.
 */
public class AssistInfo {

    @JsonIgnore
    private TeamDTO assistFor;
    private int playerId;
    private String playerName;
    private String type;
    private String time;

    public TeamDTO getAssistFor() {
        return assistFor;
    }

    public void setAssistFor(TeamDTO assistFor) {
        this.assistFor = assistFor;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
