package pl.bmiedlar.stats.output;

import java.util.List;
import java.util.Map;

/**
 * Created by Bartosz on 07.09.2015.
 */
public class PenaltiesInfo {

    private int idMatch;
    private Map<Integer, List<PenaltyInfo>> penaltiesInfo;

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public Map<Integer, List<PenaltyInfo>> getPenaltiesInfo() {
        return penaltiesInfo;
    }

    public void setPenaltiesInfo(Map<Integer, List<PenaltyInfo>> penaltiesInfo) {
        this.penaltiesInfo = penaltiesInfo;
    }
}
