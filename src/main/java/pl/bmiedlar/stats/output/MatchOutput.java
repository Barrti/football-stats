package pl.bmiedlar.stats.output;

import pl.bmiedlar.stats.match.MatchLineupDTO;

/**
 * Created by bmiedlar on 2015-08-17.
 */
public class MatchOutput {

    private int matchId;
    private MatchLineupDTO matchInfo;
    private TimeOnPitch timeOnPitch;
    private ScoreBoard scoreBoard;
    private GoalsInfo goalsInfo;
    private AssistsInfo assistsInfo;
    private CardsInfo cardsInfo;
    private PenaltiesInfo penaltiesInfo;
    private SubstitutesInfo substitutesInfo;

    public TimeOnPitch getTimeOnPitch() {
        return timeOnPitch;
    }

    public void setTimeOnPitch(TimeOnPitch timeOnPitch) {
        this.timeOnPitch = timeOnPitch;
    }

    public PenaltiesInfo getPenaltiesInfo() {
        return penaltiesInfo;
    }

    public void setPenaltiesInfo(PenaltiesInfo penaltiesInfo) {
        this.penaltiesInfo = penaltiesInfo;
    }

    public int getMatchId() {
        return matchId;
    }

    public void setMatchId(int matchId) {
        this.matchId = matchId;
    }

    public GoalsInfo getGoalsInfo() {
        return goalsInfo;
    }

    public void setGoalsInfo(GoalsInfo goalsInfo) {
        this.goalsInfo = goalsInfo;
    }

    public MatchLineupDTO getMatchInfo() {
        return matchInfo;
    }

    public void setMatchInfo(MatchLineupDTO matchInfo) {
        this.matchInfo = matchInfo;
    }

    public AssistsInfo getAssistsInfo() {
        return assistsInfo;
    }

    public void setAssistsInfo(AssistsInfo assistsInfo) {
        this.assistsInfo = assistsInfo;
    }

    public SubstitutesInfo getSubstitutesInfo() {
        return substitutesInfo;
    }

    public void setSubstitutesInfo(SubstitutesInfo substitutesInfo) {
        this.substitutesInfo = substitutesInfo;
    }

    public CardsInfo getCardsInfo() {
        return cardsInfo;
    }

    public void setCardsInfo(CardsInfo cardsInfo) {
        this.cardsInfo = cardsInfo;
    }

    public ScoreBoard getScoreBoard() {
        return scoreBoard;
    }

    public void setScoreBoard(ScoreBoard scoreBoard) {
        this.scoreBoard = scoreBoard;
    }

}
