package pl.bmiedlar.stats.output;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bmiedlar.stats.events.Card;
import pl.bmiedlar.stats.events.CardRepository;
import pl.bmiedlar.stats.events.Substitute;
import pl.bmiedlar.stats.events.SubstituteRepository;
import pl.bmiedlar.stats.info.Player;
import pl.bmiedlar.stats.info.PlayerTime;
import pl.bmiedlar.stats.match.Lineup;
import pl.bmiedlar.stats.match.LineupRepository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Service
public class TimeService {

    @Autowired
    LineupRepository lineupRepository;

    @Autowired
    SubstituteRepository substituteRepository;

    @Autowired
    CardRepository cardRepository;

    private static final String MINUTES = "90:00";

    public TimeOnPitch createToP(int idMatch) {
        TimeOnPitch top = new TimeOnPitch();
        top.setPlayerTimes(createTimes(idMatch));
        top.setIdMatch(idMatch);

        return top;
    }

    private Map<Integer, PlayerTime> createTimes(int idMatch) {
        List<Lineup> lineups = lineupRepository.findByMatchId(idMatch);
        List<Player> startingPlayers = new ArrayList<>();
        List<Player> benchPlayers = new ArrayList<>();
        Map<Integer, PlayerTime> playersTime = new LinkedHashMap<>();
        List<Substitute> substitutes = new ArrayList<>(substituteRepository.findByMatchId(idMatch));
        List<Card> cards = new ArrayList<>(cardRepository.findByMatchId(idMatch));
        lineups.forEach(lineup -> {
            lineup.getStartingEleven().forEach(startingPlayers::add);
            lineup.getBench().forEach(benchPlayers::add);
        });
        startingPlayers.forEach(startingPlayer -> {
            PlayerTime pt = new PlayerTime();
            pt.setPlayerId(startingPlayer.getId());
            pt.setPlayerName(startingPlayer.getFirstName() + " " + startingPlayer.getLastName());
            pt.setTimeStart("00:00");
            pt.setTimeOff(MINUTES);
            if(!substitutes.isEmpty()) {
                substitutes.forEach(substitute -> {
                    if (startingPlayer.getId() == substitute.getPlayerOut().getId())
                        pt.setTimeOff(substitute.getTime());
                    else
                        pt.setTimeOff(MINUTES);
                });
            }
            cards.forEach(card -> {
                if (card.getType().contains("Czerwona") && card.getPlayer().getId() == startingPlayer.getId())
                    pt.setTimeOff(card.getTime());
            });
            String[] stopParts = pt.getTimeOff().split(":");
            int timeOnPitch = Integer.parseInt(stopParts[0])*60 + Integer.parseInt(stopParts[1]);
            pt.setTimeOnPitch(String.valueOf(timeOnPitch / 60) + ":" + String.format("%02d", timeOnPitch % 60));
            playersTime.put(pt.getPlayerId(), pt);
        });

        benchPlayers.forEach(benchPlayer -> {
            if(!substitutes.isEmpty()) {
                substitutes.forEach(substitute -> {
                    if (benchPlayer.getId() == substitute.getPlayerIn().getId()) {
                        PlayerTime pt = new PlayerTime();
                        pt.setPlayerId(benchPlayer.getId());
                        pt.setPlayerName(benchPlayer.getFirstName() + " " + benchPlayer.getLastName());
                        pt.setTimeStart(substitute.getTime());
                        substitutes.forEach(substitute1 -> {
                            if (benchPlayer.getId() == substitute.getPlayerOut().getId())
                                pt.setTimeOff(substitute.getTime());
                            else
                                pt.setTimeOff(MINUTES);
                        });
                        cards.forEach(card -> {
                            if (card.getType().toUpperCase().contains("CZERWONA") && card.getPlayer().getId() == benchPlayer.getId())
                                pt.setTimeOff(card.getTime());
                        });
                        String[] startParts = pt.getTimeStart().split(":");
                        String[] stopParts = pt.getTimeOff().split(":");
                        Integer timeOnPitch = Integer.parseInt(stopParts[0]) * 60 + Integer.parseInt(stopParts[1]) - (Integer.parseInt(startParts[0]) * 60 + Integer.parseInt(startParts[1]));
                        pt.setTimeOnPitch(String.valueOf(timeOnPitch / 60) + ":" + String.format("%02d", timeOnPitch % 60));
                        playersTime.put(pt.getPlayerId(), pt);
                    }
                });
            }
        });

        return playersTime;
    }
}
