package pl.bmiedlar.stats.output;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.bmiedlar.stats.info.TeamDTO;

/**
 * Created by bmiedlar on 2015-08-17.
 */
public class SubstituteInfo {

    @JsonIgnore
    private TeamDTO team;
    private String time;
    private int playerInId;
    private String playerIn;
    private int playerOutId;
    private String playerOut;

    public TeamDTO getTeam() {
        return team;
    }

    public void setTeam(TeamDTO team) {
        this.team = team;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPlayerIn() {
        return playerIn;
    }

    public void setPlayerIn(String playerIn) {
        this.playerIn = playerIn;
    }

    public String getPlayerOut() {
        return playerOut;
    }

    public void setPlayerOut(String playerOut) {
        this.playerOut = playerOut;
    }

    public int getPlayerInId() {
        return playerInId;
    }

    public void setPlayerInId(int playerInId) {
        this.playerInId = playerInId;
    }

    public int getPlayerOutId() {
        return playerOutId;
    }

    public void setPlayerOutId(int playerOutId) {
        this.playerOutId = playerOutId;
    }
}
