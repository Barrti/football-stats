package pl.bmiedlar.stats.output;

import java.util.List;
import java.util.Map;

public class AssistsInfo {

    private int idMatch;
    private Map<Integer, List<AssistInfo>> assistInfo;

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public Map<Integer, List<AssistInfo>> getAssistInfo() {
        return assistInfo;
    }

    public void setAssistInfo(Map<Integer, List<AssistInfo>> assistInfo) {
        this.assistInfo = assistInfo;
    }
}
