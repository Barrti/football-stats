package pl.bmiedlar.stats.output;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.bmiedlar.stats.info.TeamDTO;

/**
 * Created by bmiedlar on 2015-08-17.
 */
public class GoalInfo {

    @JsonIgnore
    private TeamDTO goalFor;
    private int playerId;
    private String player;
    private String type;
    private String time;

    public TeamDTO getGoalFor() {
        return goalFor;
    }

    public void setGoalFor(TeamDTO goalFor) {
        this.goalFor = goalFor;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
