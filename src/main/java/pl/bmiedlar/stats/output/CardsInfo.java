package pl.bmiedlar.stats.output;

import java.util.List;
import java.util.Map;

/**
 * Created by Bartosz on 07.09.2015.
 */
public class CardsInfo {

    private int idMatch;
    private Map<Integer, List<CardInfo>> cardInfo;

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public Map<Integer, List<CardInfo>> getCardInfo() {
        return cardInfo;
    }

    public void setCardInfo(Map<Integer, List<CardInfo>> cardInfo) {
        this.cardInfo = cardInfo;
    }
}
