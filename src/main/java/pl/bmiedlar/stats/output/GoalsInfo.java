package pl.bmiedlar.stats.output;

import java.util.List;
import java.util.Map;

public class GoalsInfo {

    private int idMatch;
    private Map<Integer, List<GoalInfo>> goalInfo;

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public Map<Integer, List<GoalInfo>> getGoalInfo() {
        return goalInfo;
    }

    public void setGoalInfo(Map<Integer, List<GoalInfo>> goalInfo) {
        this.goalInfo = goalInfo;
    }
}
