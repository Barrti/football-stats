package pl.bmiedlar.stats.output;

import com.fasterxml.jackson.annotation.JsonIgnore;
import pl.bmiedlar.stats.info.TeamDTO;

/**
 * Created by Bartosz on 27.08.2015.
 */
public class PenaltyInfo {

    @JsonIgnore
    private TeamDTO penaltyFor;
    private String time;
    private int playerFouledId;
    private String playerFouled;
    private int playerFoulerId;
    private String playerFouler;
    private int playerExecutorId;
    private String playerExecutor;
    private String type;
    private String cause;

    public TeamDTO getPenaltyFor() {
        return penaltyFor;
    }

    public void setPenaltyFor(TeamDTO penaltyFor) {
        this.penaltyFor = penaltyFor;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPlayerFouled() {
        return playerFouled;
    }

    public void setPlayerFouled(String playerFouled) {
        this.playerFouled = playerFouled;
    }

    public String getPlayerFouler() {
        return playerFouler;
    }

    public void setPlayerFouler(String playerFouler) {
        this.playerFouler = playerFouler;
    }

    public String getPlayerExecutor() {
        return playerExecutor;
    }

    public void setPlayerExecutor(String playerExecutor) {
        this.playerExecutor = playerExecutor;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public int getPlayerFouledId() {
        return playerFouledId;
    }

    public void setPlayerFouledId(int playerFouledId) {
        this.playerFouledId = playerFouledId;
    }

    public int getPlayerFoulerId() {
        return playerFoulerId;
    }

    public void setPlayerFoulerId(int playerFoulerId) {
        this.playerFoulerId = playerFoulerId;
    }

    public int getPlayerExecutorId() {
        return playerExecutorId;
    }

    public void setPlayerExecutorId(int playerExecutorId) {
        this.playerExecutorId = playerExecutorId;
    }
}
