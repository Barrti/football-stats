package pl.bmiedlar.stats.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Bartosz on 11.08.2015.
 */
@Component
public class PlayerWrapper {

    @Autowired
    TeamRepository teamRepository;

    public Player wrapToEntity(PlayerDTO dto) {
        Player player = new Player();

        player.setFirstName(dto.getFirstName());
        player.setLastName(dto.getLastName());
        player.setNumber(dto.getNumber());
        player.setTeam(teamRepository.findOne(dto.getIdTeam()));
        player.setId(dto.getId());
        player.setPosition(dto.getPosition());

        return player;
    }

    public PlayerDTO wrapToDTO(Player player) {
        PlayerDTO dto = new PlayerDTO();

        dto.setId(player.getId());
        dto.setNumber(player.getNumber());
        dto.setLastName(player.getLastName());
        dto.setFirstName(player.getFirstName());
        dto.setPosition(player.getPosition());
        dto.setIdTeam(player.getTeam().getId());

        return dto;
    }
}
