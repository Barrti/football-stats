package pl.bmiedlar.stats.info;

import org.springframework.stereotype.Component;

/**
 * Created by bmiedlar on 2015-08-11.
 */
@Component
public class MatchdayWrapper {

    public Matchday wrapToEntity(MatchdayDTO dto) {
        Matchday matchday = new Matchday();

        matchday.setName(dto.getName());
        return matchday;
    }

    public MatchdayDTO wrapToDTO(Matchday matchday) {
        MatchdayDTO dto = new MatchdayDTO();

        dto.setName(matchday.getName());
        dto.setId(matchday.getId());

        return dto;
    }
}