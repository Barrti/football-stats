package pl.bmiedlar.stats.info;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Created by Bartosz on 28.08.2015.
 */
public class PlayerTime {

    @JsonIgnore
    private int playerId;
    private String playerName;
    private String timeStart;
    private String timeOff;
    private String timeOnPitch;

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getTimeStart() {
        return timeStart;
    }

    public void setTimeStart(String timeStart) {
        this.timeStart = timeStart;
    }

    public String getTimeOff() {
        return timeOff;
    }

    public void setTimeOff(String timeOff) {
        this.timeOff = timeOff;
    }

    public String getTimeOnPitch() {
        return timeOnPitch;
    }

    public void setTimeOnPitch(String timeOnPitch) {
        this.timeOnPitch = timeOnPitch;
    }
}
