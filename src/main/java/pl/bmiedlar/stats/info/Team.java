package pl.bmiedlar.stats.info;

import javax.persistence.*;

/**
 * Created by bmiedlar on 2015-08-10.
 */

@Table
@Entity
public class Team {

    @Id @GeneratedValue(strategy = GenerationType.TABLE)
    @Column
    private int id;

    @Column
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
