package pl.bmiedlar.stats.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Bartosz on 11.08.2015.
 */
@Service
public class PlayerService {

    @Autowired
    PlayerWrapper playerWrapper;

    @Autowired
    PlayerRepository playerRepository;

    public PlayerDTO savePlayer(PlayerDTO dto) {
        return playerWrapper.wrapToDTO(playerRepository.save(playerWrapper.wrapToEntity(dto)));
    }

    public Set<PlayerDTO> getAllPlayers() {
        Set<Player> players = new HashSet<>(playerRepository.findAll());
        Set<PlayerDTO> dtos = new LinkedHashSet<>();

        players.forEach(player -> dtos.add(playerWrapper.wrapToDTO(player)));

        return dtos;
    }

    public PlayerDTO getPlayerById(int id) {
        return playerWrapper.wrapToDTO(playerRepository.findOne(id));
    }

    public Set<PlayerDTO> getByTeam(int id) {
        Set<Player> players = new LinkedHashSet<>(playerRepository.findByTeamId(id));
        Set<PlayerDTO> dtos = new LinkedHashSet<>();

        players.forEach(player -> dtos.add(playerWrapper.wrapToDTO(player)));

        return dtos;
    }

    public PlayerDTO updatePlayer(PlayerDTO dto) {
        return playerWrapper.wrapToDTO(playerRepository.save(playerWrapper.wrapToEntity(dto)));
    }
}
