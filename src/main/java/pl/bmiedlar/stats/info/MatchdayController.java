package pl.bmiedlar.stats.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-11.
 */
@RestController
public class MatchdayController {

    @Autowired
    MatchdayService matchdayService;

    @Autowired
    TeamService teamService;

    @Autowired
    PlayerService playerService;

    // MATCHDAY START

    @RequestMapping(value = "/api/matchdays", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<MatchdayDTO> findAllMatchdays() {
        return matchdayService.getAllMatchdays();
    }

    @RequestMapping(value = "/api/matchdays", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public MatchdayDTO addMatchday(@RequestBody @Valid MatchdayDTO dto) {
        return matchdayService.saveMatchday(dto);
    }

    @RequestMapping(value = "/api/matchdays/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public MatchdayDTO findMatchdayById(@PathVariable(value = "id") @Valid int id) {
        return matchdayService.getMatchdayById(id);
    }

    @RequestMapping(value = "/api/matchdays/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public MatchdayDTO updateMatchday(@PathVariable(value = "id") @Valid int id, @RequestBody @Valid MatchdayDTO dto) {
        return matchdayService.updateMatchday(dto);
    }

    // MATCHDAY END
    // TEAM START

    @RequestMapping(value = "/api/teams", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<TeamDTO> findAllTeams() {
        return teamService.getAllTeams();
    }

    @RequestMapping(value = "/api/teams", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public TeamDTO addTeam(@RequestBody @Valid TeamDTO dto) {
        return teamService.saveTeam(dto);
    }

    @RequestMapping(value = "/api/teams/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public TeamDTO findTeamById(@PathVariable(value = "id") @Valid int id) {
        return teamService.getTeamById(id);
    }

    @RequestMapping(value = "/api/teams/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TeamDTO updateTeam(@PathVariable(value = "id") @Valid int id, @RequestBody @Valid TeamDTO dto) {
        return teamService.updateTeam(dto);
    }

    // TEAM END
    // PLAYER START

    @RequestMapping(value = "/api/players", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<PlayerDTO> findAllPlayers() {
        return playerService.getAllPlayers();
    }

    @RequestMapping(value = "/api/players", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public PlayerDTO addPlayer(@RequestBody @Valid PlayerDTO dto) {
        return playerService.savePlayer(dto);
    }

    @RequestMapping(value = "/api/players/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public PlayerDTO findPlayerById(@PathVariable(value = "id") @Valid int id) {
        return playerService.getPlayerById(id);
    }

    @RequestMapping(value = "/api/teams/{id}/players", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<PlayerDTO> findByTeam(@PathVariable(value = "id") int id) {
        return playerService.getByTeam(id);
    }

    @RequestMapping(value = "/api/players/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public PlayerDTO updatePlayer(@PathVariable(value = "id") @Valid int id, @RequestBody @Valid PlayerDTO dto) {
        return playerService.updatePlayer(dto);
    }

}
