package pl.bmiedlar.stats.info;

import org.springframework.stereotype.Component;

/**
 * Created by bmiedlar on 2015-08-11.
 */
@Component
public class TeamWrapper {

    public Team wrapToEntity(TeamDTO dto){
        Team team = new Team();

        team.setId(dto.getId());
        team.setName(dto.getName());

        return team;
    }

    public TeamDTO wrapToDTO(Team team) {
        TeamDTO dto = new TeamDTO();

        dto.setId(team.getId());
        dto.setName(team.getName());

        return dto;
    }
}
