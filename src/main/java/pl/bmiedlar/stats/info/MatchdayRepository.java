package pl.bmiedlar.stats.info;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by bmiedlar on 2015-08-10.
 */
public interface MatchdayRepository extends JpaRepository<Matchday, Integer> {
}
