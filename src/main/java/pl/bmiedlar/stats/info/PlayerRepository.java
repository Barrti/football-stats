package pl.bmiedlar.stats.info;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-10.
 */
public interface PlayerRepository extends JpaRepository<Player, Integer> {

    Set<Player> findByTeamId(int id);

}
