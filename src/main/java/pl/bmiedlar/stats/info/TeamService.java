package pl.bmiedlar.stats.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-11.
 */
@Service
public class TeamService {

    @Autowired
    TeamWrapper teamWrapper;

    @Autowired
    TeamRepository teamRepository;

    public TeamDTO saveTeam(TeamDTO dto) {
        return teamWrapper.wrapToDTO(teamRepository.save(teamWrapper.wrapToEntity(dto)));
    }

    public Set<TeamDTO> getAllTeams() {
        Set<Team> teams = new HashSet<>(teamRepository.findAll());
        Set<TeamDTO> dtos = new LinkedHashSet<>();

        teams.forEach(team -> dtos.add(teamWrapper.wrapToDTO(team)));

        return dtos;
    }

    public TeamDTO getTeamById(int id) {
        return teamWrapper.wrapToDTO(teamRepository.findOne(id));
    }

    public TeamDTO updateTeam(TeamDTO dto) {
        Team team = teamRepository.findOne(dto.getId());
        team.setName(dto.getName());

        return teamWrapper.wrapToDTO(teamRepository.save(team));
    }
}
