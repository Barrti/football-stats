package pl.bmiedlar.stats.info;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-11.
 */
@Service
public class MatchdayService {

    @Autowired
    MatchdayRepository matchdayRepository;

    @Autowired
    MatchdayWrapper matchdayWrapper;


    public MatchdayDTO saveMatchday(MatchdayDTO dto) {
        return matchdayWrapper.wrapToDTO(matchdayRepository.save(matchdayWrapper.wrapToEntity(dto)));
    }

    public Set<MatchdayDTO> getAllMatchdays() {
        Set<Matchday> matchdays = new HashSet<>(matchdayRepository.findAll());
        Set<MatchdayDTO> dtos = new LinkedHashSet<>();

        matchdays.forEach(matchday -> dtos.add(matchdayWrapper.wrapToDTO(matchday)));

        return dtos;
    }

    public MatchdayDTO getMatchdayById(int id) {
        return matchdayWrapper.wrapToDTO(matchdayRepository.findOne(id));
    }

    public MatchdayDTO updateMatchday(MatchdayDTO dto) {
        Matchday matchday = matchdayRepository.findOne(dto.getId());
        matchday.setName(dto.getName());

        return matchdayWrapper.wrapToDTO(matchdayRepository.save(matchday));
    }

}
