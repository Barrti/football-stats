package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Service
public class SubstituteService {

    @Autowired
    SubstituteRepository substituteRepository;

    @Autowired
    SubstituteWrapper substituteWrapper;

    public Set<SubstituteDTO> getAllChanges() {
        Set<Substitute> substitutes = new LinkedHashSet<>(substituteRepository.findAll());
        Set<SubstituteDTO> dtos = new LinkedHashSet<>();

        substitutes.forEach(change -> dtos.add(substituteWrapper.wrapToDTO(change)));

        return dtos;
    }

    public Set<SubstituteDTO> getChangesByMatch(int id) {
        Set<Substitute> substitutes = new LinkedHashSet<>(substituteRepository.findByMatchId(id));
        Set<SubstituteDTO> dtos = new LinkedHashSet<>();

        substitutes.forEach(change -> dtos.add(substituteWrapper.wrapToDTO(change)));

        return dtos;
    }

    public SubstituteDTO saveChange(SubstituteDTO dto) {
        return substituteWrapper.wrapToDTO(substituteRepository.save(substituteWrapper.wrapToEntity(dto)));
    }

    public SubstituteDTO findChangeById(int id) {
        return substituteWrapper.wrapToDTO(substituteRepository.findOne(id));
    }

    public SubstituteDTO updateChange(int id, SubstituteDTO dto) {
        Substitute substitute = substituteWrapper.wrapToEntity(dto);
        substitute.setId(id);

        return substituteWrapper.wrapToDTO(substituteRepository.save(substitute));
    }

    public void removeSubstitute(int id) {
        substituteRepository.delete(id);
    }
}
