package pl.bmiedlar.stats.events;

import pl.bmiedlar.stats.match.Match;

import javax.persistence.*;

/**
 * Created by Bartosz on 21.08.2015.
 */
@Table
@Entity
public class Score {

    @Id @GeneratedValue(strategy = GenerationType.TABLE)
    @Column
    private int id;

    @ManyToOne
    private Match match;

    @Column
    private int homeScore;

    @Column
    private int awayScore;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public int getHomeScore() {
        return homeScore;
    }

    public void setHomeScore(int homeScore) {
        this.homeScore = homeScore;
    }

    public int getAwayScore() {
        return awayScore;
    }

    public void setAwayScore(int awayScore) {
        this.awayScore = awayScore;
    }
}
