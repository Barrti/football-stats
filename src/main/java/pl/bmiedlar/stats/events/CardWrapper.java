package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bmiedlar.stats.info.PlayerRepository;
import pl.bmiedlar.stats.info.TeamRepository;
import pl.bmiedlar.stats.match.MatchRepository;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Component
public class CardWrapper {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    MatchRepository matchRepository;

    public Card wrapToEntity(CardDTO dto) {
        Card card = new Card();

        card.setPlayer(playerRepository.findOne(dto.getIdPlayer()));
        card.setTeam(teamRepository.findOne(dto.getIdTeam()));
        card.setTime(dto.getTime());
        card.setMatch(matchRepository.findOne(dto.getIdMatch()));
        card.setType(dto.getType());

        return card;
    }

    public CardDTO wrapToDTO(Card card) {
        CardDTO dto = new CardDTO();

        dto.setId(card.getId());
        dto.setType(card.getType());
        dto.setTime(card.getTime());
        dto.setIdPlayer(card.getPlayer().getId());
        dto.setIdTeam(card.getTeam().getId());
        dto.setIdMatch(card.getMatch().getId());

        return dto;
    }
}
