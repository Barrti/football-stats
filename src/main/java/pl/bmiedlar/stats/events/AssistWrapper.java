package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bmiedlar.stats.info.PlayerRepository;
import pl.bmiedlar.stats.info.TeamRepository;
import pl.bmiedlar.stats.match.MatchRepository;

/**
 * Created by bmiedlar on 2015-08-13.
 */
@Component
public class AssistWrapper {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    MatchRepository matchRepository;

    public Assist wrapToEntity(AssistDTO dto) {
        Assist assist = new Assist();

        assist.setPlayer(playerRepository.findOne(dto.getIdPlayer()));
        assist.setTeam(teamRepository.findOne(dto.getIdTeam()));
        assist.setTime(dto.getTime());
        assist.setMatch(matchRepository.findOne(dto.getIdMatch()));
        if(dto.getType().isEmpty())
            assist.setType(playerRepository.findOne(dto.getIdPlayer()).getPosition());
        else
            assist.setType(dto.getType());

        return assist;
    }

    public AssistDTO wrapToDTO(Assist assist) {
        AssistDTO dto = new AssistDTO();

        dto.setId(assist.getId());
        dto.setType(assist.getType());
        dto.setTime(assist.getTime());
        dto.setIdPlayer(assist.getPlayer().getId());
        dto.setIdTeam(assist.getTeam().getId());
        dto.setIdMatch(assist.getMatch().getId());

        return dto;
    }
}
