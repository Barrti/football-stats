package pl.bmiedlar.stats.events;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by Bartosz on 14.08.2015.
 */
public interface SubstituteRepository extends JpaRepository<Substitute, Integer> {

    Set<Substitute> findByMatchId(int id);
}
