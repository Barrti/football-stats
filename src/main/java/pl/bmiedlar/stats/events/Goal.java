package pl.bmiedlar.stats.events;

import pl.bmiedlar.stats.info.Player;
import pl.bmiedlar.stats.info.Team;
import pl.bmiedlar.stats.match.Match;

import javax.persistence.*;

/**
 * Created by bmiedlar on 2015-08-13.
 */
@Entity
@Table
public class Goal {

    @Id @GeneratedValue(strategy = GenerationType.TABLE)
    @Column
    private int id;

    @Column
    private String time;

    @Column
    private String type;

    @ManyToOne
    private Player player;

    @ManyToOne
    private Team team;

    @ManyToOne
    private Match match;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
