package pl.bmiedlar.stats.events;

/**
 * Created by Bartosz on 14.08.2015.
 */
public class SubstituteDTO {

    private int id;
    private int idMatch;
    private int idPlayerIn;
    private int idPlayerOut;
    private int idTeam;
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public int getIdPlayerIn() {
        return idPlayerIn;
    }

    public void setIdPlayerIn(int idPlayerIn) {
        this.idPlayerIn = idPlayerIn;
    }

    public int getIdPlayerOut() {
        return idPlayerOut;
    }

    public void setIdPlayerOut(int idPlayerOut) {
        this.idPlayerOut = idPlayerOut;
    }

    public int getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(int idTeam) {
        this.idTeam = idTeam;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
