package pl.bmiedlar.stats.events;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Bartosz on 21.08.2015.
 */
public interface ScoreRepository extends JpaRepository<Score, Integer> {

    Score findByMatchId(int matchId);
}
