package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bmiedlar.stats.match.MatchRepository;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-13.
 */
@Service
public class GoalService {

    @Autowired
    GoalWrapper goalWrapper;

    @Autowired
    GoalRepository goalRepository;

    @Autowired
    ScoreService scoreService;

    @Autowired
    MatchRepository matchRepository;

    public GoalDTO saveGoal(GoalDTO dto) {
        GoalDTO goal = goalWrapper.wrapToDTO(goalRepository.save(goalWrapper.wrapToEntity(dto)));
        if(goal.getIdTeam() == matchRepository.findOne(goal.getIdMatch()).getHomeTeam().getId())
            scoreService.homeScored(matchRepository.findOne(goal.getIdMatch()).getId());
        else if(goal.getIdTeam() == matchRepository.findOne(goal.getIdMatch()).getAwayTeam().getId())
            scoreService.awayScored(matchRepository.findOne(goal.getIdMatch()).getId());
        return goal;
    }

    public Set<GoalDTO> getAllGoals() {
        Set<Goal> goals = new LinkedHashSet<>(goalRepository.findAll());
        Set<GoalDTO> dtos = new LinkedHashSet<>();

        goals.forEach(goal -> dtos.add(goalWrapper.wrapToDTO(goal)));

        return dtos;
    }

    public GoalDTO findGoalById(int id) {
        return goalWrapper.wrapToDTO(goalRepository.findOne(id));
    }

    public Set<GoalDTO> findByMatchId(int id) {
        Set<Goal> goals = new LinkedHashSet<>(goalRepository.findByMatchId(id));
        Set<GoalDTO> dtos = new LinkedHashSet<>();

        goals.forEach(goal -> dtos.add(goalWrapper.wrapToDTO(goal)));

        return dtos;
    }

    public GoalDTO updateGoal(GoalDTO dto, int id) {
        Goal goal = goalWrapper.wrapToEntity(dto);
        goal.setId(id);

        return goalWrapper.wrapToDTO(goalRepository.save(goal));
    }

    public void deleteGoal(int id) {
        Goal goal = goalRepository.findOne(id);

        if(goal.getTeam().getId() == matchRepository.findOne(goal.getMatch().getId()).getHomeTeam().getId())
            scoreService.removeHome(matchRepository.findOne(goal.getMatch().getId()).getId());
        else if(goal.getTeam().getId() == matchRepository.findOne(goal.getMatch().getId()).getAwayTeam().getId())
            scoreService.removeAway(matchRepository.findOne(goal.getMatch().getId()).getId());

        goalRepository.delete(id);

    }
}
