package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bmiedlar.stats.info.PlayerRepository;
import pl.bmiedlar.stats.info.TeamRepository;
import pl.bmiedlar.stats.match.MatchRepository;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Component
public class PenaltyWrapper {

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    MatchRepository matchRepository;

    public Penalty wrapToEntity(PenaltyDTO dto) {
        Penalty penalty = new Penalty();

        penalty.setTeam(teamRepository.findOne(dto.getIdTeam()));
        penalty.setCause(playerRepository.findOne(dto.getIdFouled()).getPosition());
        penalty.setExecutor(playerRepository.findOne(dto.getIdExecutor()));
        penalty.setFouler(playerRepository.findOne(dto.getIdFouler()));
        penalty.setMatch(matchRepository.findOne(dto.getIdMatch()));
        penalty.setFouled(playerRepository.findOne(dto.getIdFouled()));
        penalty.setTime(dto.getTime());
        penalty.setType(dto.getType());

        return penalty;
    }

    public PenaltyDTO wrapToDTO(Penalty penalty) {
        PenaltyDTO dto = new PenaltyDTO();

        dto.setId(penalty.getId());
        dto.setType(penalty.getType());
        dto.setTime(penalty.getTime());
        dto.setCause(penalty.getCause());
        dto.setIdExecutor(penalty.getExecutor().getId());
        dto.setIdFouler(penalty.getFouler().getId());
        dto.setIdMatch(penalty.getMatch().getId());
        dto.setIdTeam(penalty.getTeam().getId());
        dto.setIdFouled(penalty.getFouled().getId());

        return dto;
    }
}
