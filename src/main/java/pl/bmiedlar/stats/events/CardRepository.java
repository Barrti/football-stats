package pl.bmiedlar.stats.events;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-13.
 */
public interface CardRepository extends JpaRepository<Card, Integer> {

    Set<Card> findByMatchId(int id);

}
