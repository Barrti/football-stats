package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.bmiedlar.stats.match.MatchRepository;

/**
 * Created by Bartosz on 21.08.2015.
 */
@Service
public class ScoreService {

    @Autowired
    ScoreRepository scoreRepository;

    @Autowired
    MatchRepository matchRepository;

    public Score createScore(int matchId) {
        Score score = new Score();
        score.setMatch(matchRepository.findOne(matchId));
        score.setHomeScore(0);
        score.setAwayScore(0);
        return scoreRepository.save(score);
    }

    public Score homeScored(int matchId) {
        Score score = scoreRepository.findByMatchId(matchId);
        score.setHomeScore(score.getHomeScore() + 1);
        return scoreRepository.save(score);
    }

    public Score awayScored(int matchId) {
        Score score = scoreRepository.findByMatchId(matchId);
        score.setAwayScore(score.getAwayScore() + 1);
        return scoreRepository.save(score);
    }

    public Score removeHome(int matchId) {
        Score score = scoreRepository.findByMatchId(matchId);
        score.setHomeScore(score.getHomeScore() - 1);
        return scoreRepository.save(score);
    }

    public Score removeAway(int matchId) {
        Score score = scoreRepository.findByMatchId(matchId);
        score.setAwayScore(score.getAwayScore()-1);
        return scoreRepository.save(score);
    }
}
