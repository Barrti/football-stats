package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-13.
 */
@Service
public class AssistService {

    @Autowired
    AssistWrapper assistWrapper;

    @Autowired
    AssistRepository assistRepository;

    public AssistDTO addAssist(AssistDTO dto) {
        return assistWrapper.wrapToDTO(assistRepository.save(assistWrapper.wrapToEntity(dto)));
    }

    public Set<AssistDTO> getAllAssists() {
        Set<Assist> assists = new LinkedHashSet<>(assistRepository.findAll());
        Set<AssistDTO> dtos = new LinkedHashSet<>();

        assists.forEach(assist -> dtos.add(assistWrapper.wrapToDTO(assist)));

        return dtos;
    }

    public AssistDTO getAssistsById(int id) {
        return assistWrapper.wrapToDTO(assistRepository.findOne(id));
    }

    public AssistDTO updateAssist(AssistDTO dto, int id) {
        Assist assist = assistWrapper.wrapToEntity(dto);
        assist.setId(id);

        return assistWrapper.wrapToDTO(assistRepository.save(assist));
    }

    public Set<AssistDTO> getAssistsByMatch(int id) {
        Set<Assist> assists = assistRepository.findByMatchId(id);
        Set<AssistDTO> dtos = new LinkedHashSet<>();

        assists.forEach(assist -> dtos.add(assistWrapper.wrapToDTO(assist)));

        return dtos;
    }

    public void removeAssist(int id) {
        assistRepository.delete(id);
    }

}
