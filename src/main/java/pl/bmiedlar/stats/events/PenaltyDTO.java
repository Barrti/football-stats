package pl.bmiedlar.stats.events;

/**
 * Created by Bartosz on 14.08.2015.
 */
public class PenaltyDTO {

    private int id;
    private int idExecutor;
    private int idFouler;
    private int idFouled;
    private int idTeam;
    private int idMatch;
    private String time;
    private String cause;
    private String type;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdExecutor() {
        return idExecutor;
    }

    public void setIdExecutor(int idExecutor) {
        this.idExecutor = idExecutor;
    }

    public int getIdFouler() {
        return idFouler;
    }

    public void setIdFouler(int idFouler) {
        this.idFouler = idFouler;
    }

    public int getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(int idTeam) {
        this.idTeam = idTeam;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public int getIdFouled() {
        return idFouled;
    }

    public void setIdFouled(int idFouled) {
        this.idFouled = idFouled;
    }
}
