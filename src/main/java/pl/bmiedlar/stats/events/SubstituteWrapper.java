package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bmiedlar.stats.info.PlayerRepository;
import pl.bmiedlar.stats.info.TeamRepository;
import pl.bmiedlar.stats.match.MatchRepository;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Component
public class SubstituteWrapper {

    @Autowired
    MatchRepository matchRepository;
    @Autowired
    PlayerRepository playerRepository;
    @Autowired
    TeamRepository teamRepository;

    public Substitute wrapToEntity(SubstituteDTO dto) {
        Substitute substitute = new Substitute();

        substitute.setTime(dto.getTime());
        substitute.setMatch(matchRepository.findOne(dto.getIdMatch()));
        substitute.setPlayerIn(playerRepository.findOne(dto.getIdPlayerIn()));
        substitute.setPlayerOut(playerRepository.findOne(dto.getIdPlayerOut()));
        substitute.setTeam(teamRepository.findOne(dto.getIdTeam()));

        return substitute;
    }

    public SubstituteDTO wrapToDTO(Substitute substitute) {
        SubstituteDTO dto = new SubstituteDTO();

        dto.setTime(substitute.getTime());
        dto.setIdTeam(substitute.getTeam().getId());
        dto.setId(substitute.getId());
        dto.setIdMatch(substitute.getMatch().getId());
        dto.setIdPlayerIn(substitute.getPlayerIn().getId());
        dto.setIdPlayerOut(substitute.getPlayerOut().getId());

        return dto;
    }
}
