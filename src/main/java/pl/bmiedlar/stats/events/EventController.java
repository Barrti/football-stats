package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

/**
 * Created by Bartosz on 14.08.2015.
 */

@RestController
public class EventController {

    @Autowired
    CardService cardService;

    @Autowired
    GoalService goalService;

    @Autowired
    AssistService assistService;

    @Autowired
    PenaltyService penaltyService;

    @Autowired
    SubstituteService substituteService;

    @RequestMapping(value = "/api/cards", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<CardDTO> findAllCards() {
        return cardService.getAllCards();
    }

    @RequestMapping(value = "/api/cards", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public CardDTO saveCard(@RequestBody @Valid CardDTO dto) {
        return cardService.addCart(dto);
    }

    @RequestMapping(value = "/api/cards/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public CardDTO getOneCard(@PathVariable(value = "id") int id) {
        return cardService.getCardById(id);
    }

    @RequestMapping(value = "/api/cards/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public CardDTO updateCard(@PathVariable(value = "id") int id, @RequestBody @Valid CardDTO dto) {
        return cardService.updateCard(dto, id);
    }

    @RequestMapping(value = "/api/matches/{id}/cards", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<CardDTO> findCardsByMatch(@PathVariable("id") int id) {
        return cardService.getCardByMatch(id);
    }

    @RequestMapping(value = "/api/goals", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<GoalDTO> findAllGoals() {
        return goalService.getAllGoals();
    }

    @RequestMapping(value = "/api/goals", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public GoalDTO saveGoal(@RequestBody @Valid GoalDTO dto) {
        return goalService.saveGoal(dto);
    }

    @RequestMapping(value = "/api/goals/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public GoalDTO getOneGoal(@PathVariable(value = "id") int id) {
        return goalService.findGoalById(id);
    }

    @RequestMapping(value = "/api/goals/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public GoalDTO updateGoal(@PathVariable(value = "id") int id, @RequestBody @Valid GoalDTO dto) {
        return goalService.updateGoal(dto, id);
    }

    @RequestMapping(value = "/api/matches/{id}/goals", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<GoalDTO> findGoalsByMatch(@PathVariable("id") int id) {
        return goalService.findByMatchId(id);
    }

    @RequestMapping(value = "/api/assists", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<AssistDTO> getAllAssists() {
        return assistService.getAllAssists();
    }

    @RequestMapping(value = "/api/assists", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public AssistDTO saveAssist(@RequestBody AssistDTO dto) {
        return assistService.addAssist(dto);
    }

    @RequestMapping(value = "/api/assists/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public AssistDTO getOneAssist(@PathVariable(value = "id") int id) {
        return assistService.getAssistsById(id);
    }

    @RequestMapping(value = "/api/assists/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public AssistDTO updateAssist(@PathVariable(value = "id") int id, @RequestBody AssistDTO dto) {
        return assistService.updateAssist(dto, id);
    }

    @RequestMapping(value = "/api/matches/{id}/assists", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<AssistDTO> findAssistsByMatch(@PathVariable("id") int id) {
        return assistService.getAssistsByMatch(id);
    }

    @RequestMapping(value = "/api/penalties", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<PenaltyDTO> getAllPenalties() {
        return penaltyService.findAllPenalties();
    }

    @RequestMapping(value = "/api/penalties", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public PenaltyDTO savePenalty(@RequestBody PenaltyDTO dto) {
        return penaltyService.savePenalty(dto);
    }

    @RequestMapping(value = "/api/penalties/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public PenaltyDTO getOnePenalty(@PathVariable(value = "id") int id) {
        return penaltyService.findPenaltyById(id);
    }

    @RequestMapping(value = "/api/penalties/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public PenaltyDTO updatePenalty(@PathVariable(value = "id") int id, @RequestBody PenaltyDTO dto) {
        return penaltyService.updatePenalty(id, dto);
    }

    @RequestMapping(value = "/api/matches/{id}/penalties", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<PenaltyDTO> findPenaltiesByMatch(@PathVariable("id") int id) {
        return penaltyService.findPenaltiesByMatch(id);
    }

    @RequestMapping(value = "/api/substitutes", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<SubstituteDTO> findAllChanges() {
        return substituteService.getAllChanges();
    }

    @RequestMapping(value = "/api/substitutes", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public SubstituteDTO saveChange(@RequestBody SubstituteDTO dto) {
        return substituteService.saveChange(dto);
    }

    @RequestMapping(value = "/api/substitutes/{id}", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public SubstituteDTO findOneChange(@PathVariable(value = "id") int id) {
        return substituteService.findChangeById(id);
    }

    @RequestMapping(value = "/api/substitutes/{id}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.OK)
    public SubstituteDTO updateChange(@PathVariable(value = "id") int id, @RequestBody SubstituteDTO dto) {
        return substituteService.updateChange(id, dto);
    }

    @RequestMapping(value = "/api/matches/{id}/substitutes", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public Set<SubstituteDTO> findSubstitutesByMatch(@PathVariable("id") int id) {
        return substituteService.getChangesByMatch(id);
    }

    // DELETES

    @RequestMapping(value = "/api/substitutes/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeSubstitute(@PathVariable("id") int id) {
        substituteService.removeSubstitute(id);
    }

    @RequestMapping(value = "/api/goals/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeGoal(@PathVariable("id") int id) {
        goalService.deleteGoal(id);
    }

    @RequestMapping(value = "/api/assists/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeAssist(@PathVariable("id") int id) {
        assistService.removeAssist(id);
    }

    @RequestMapping(value = "/api/penalties/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removePenalty(@PathVariable("id") int id) {
        penaltyService.removePenalty(id);
    }

    @RequestMapping(value = "/api/cards/{id}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeCards(@PathVariable("id") int id) {
        cardService.removeCard(id);
    }

}
