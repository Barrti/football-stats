package pl.bmiedlar.stats.events;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by bmiedlar on 2015-08-13.
 */
public interface GoalRepository extends JpaRepository<Goal, Integer> {

    Set<Goal> findByMatchId(int id);
}
