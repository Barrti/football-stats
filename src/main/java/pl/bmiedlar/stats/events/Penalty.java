package pl.bmiedlar.stats.events;

import pl.bmiedlar.stats.info.Player;
import pl.bmiedlar.stats.info.Team;
import pl.bmiedlar.stats.match.Match;

import javax.persistence.*;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Entity
@Table
public class Penalty {

    @Id @GeneratedValue(strategy = GenerationType.TABLE)
    @Column
    private int id;

    @Column
    private String time;

    @Column
    private String type;

    @Column
    private String cause;

    @ManyToOne
    private Player executor;

    @ManyToOne
    private Player fouler;

    @ManyToOne
    private Player fouled;

    @ManyToOne
    private Team team;

    @ManyToOne
    private Match match;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public Player getExecutor() {
        return executor;
    }

    public void setExecutor(Player executor) {
        this.executor = executor;
    }

    public Player getFouler() {
        return fouler;
    }

    public void setFouler(Player fouler) {
        this.fouler = fouler;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public Player getFouled() {
        return fouled;
    }

    public void setFouled(Player fouled) {
        this.fouled = fouled;
    }
}
