package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.bmiedlar.stats.info.PlayerRepository;
import pl.bmiedlar.stats.info.TeamRepository;
import pl.bmiedlar.stats.match.MatchRepository;

/**
 * Created by bmiedlar on 2015-08-13.
 */
@Component
public class GoalWrapper {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    MatchRepository matchRepository;

    public Goal wrapToEntity(GoalDTO dto) {
        Goal goal = new Goal();

        goal.setPlayer(playerRepository.findOne(dto.getIdPlayer()));
        goal.setTeam(teamRepository.findOne(dto.getIdTeam()));
        goal.setTime(dto.getTime());
        goal.setMatch(matchRepository.findOne(dto.getIdMatch()));
        if(dto.getType().isEmpty())
            goal.setType(playerRepository.findOne(dto.getIdPlayer()).getPosition());
        else
            goal.setType(dto.getType());

        return goal;
    }

    public GoalDTO wrapToDTO(Goal goal) {
        GoalDTO dto = new GoalDTO();

        dto.setId(goal.getId());
        dto.setType(goal.getType());
        dto.setTime(goal.getTime());
        dto.setIdPlayer(goal.getPlayer().getId());
        dto.setIdTeam(goal.getTeam().getId());
        dto.setIdMatch(goal.getMatch().getId());

        return dto;
    }
}
