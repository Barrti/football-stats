package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Bartosz on 14.08.2015.
 */

@Service
public class CardService {

    @Autowired
    CardWrapper cardWrapper;

    @Autowired
    CardRepository cardRepository;

    public CardDTO addCart(CardDTO dto) {
        return cardWrapper.wrapToDTO(cardRepository.save(cardWrapper.wrapToEntity(dto)));
    }

    public Set<CardDTO> getAllCards() {
        Set<Card> cards = new LinkedHashSet<>(cardRepository.findAll());
        Set<CardDTO> dtos = new LinkedHashSet<>();

        cards.forEach(card -> dtos.add(cardWrapper.wrapToDTO(card)));

        return dtos;
    }

    public CardDTO getCardById(int id) {
        return cardWrapper.wrapToDTO(cardRepository.findOne(id));
    }

    public CardDTO updateCard(CardDTO dto, int id) {
        Card card = cardWrapper.wrapToEntity(dto);
        card.setId(id);

        return cardWrapper.wrapToDTO(cardRepository.save(card));
    }

    public Set<CardDTO> getCardByMatch(int id) {
        Set<Card> cards = cardRepository.findByMatchId(id);
        Set<CardDTO> dtos = new LinkedHashSet<>();

        cards.forEach(card -> dtos.add(cardWrapper.wrapToDTO(card)));

        return dtos;
    }

    public void removeCard(int id) {
        cardRepository.delete(id);
    }
}
