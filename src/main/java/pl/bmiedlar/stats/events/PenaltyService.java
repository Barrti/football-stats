package pl.bmiedlar.stats.events;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Bartosz on 14.08.2015.
 */
@Service
public class PenaltyService {

    @Autowired
    PenaltyWrapper penaltyWrapper;

    @Autowired
    PenaltyRepositroy penaltyRepositroy;

    public PenaltyDTO savePenalty(PenaltyDTO dto) {
        return penaltyWrapper.wrapToDTO(penaltyRepositroy.save(penaltyWrapper.wrapToEntity(dto)));
    }

    public Set<PenaltyDTO> findAllPenalties() {
        Set<Penalty> penalties = new LinkedHashSet<>(penaltyRepositroy.findAll());
        Set<PenaltyDTO> dtos = new LinkedHashSet<>();

        penalties.forEach(penalty -> dtos.add(penaltyWrapper.wrapToDTO(penalty)));

        return dtos;
    }

    public Set<PenaltyDTO> findPenaltiesByMatch(int id) {
        Set<Penalty> penalties = new LinkedHashSet<>(penaltyRepositroy.findByMatchId(id));
        Set<PenaltyDTO> dtos = new LinkedHashSet<>();

        penalties.forEach(penalty -> dtos.add(penaltyWrapper.wrapToDTO(penalty)));

        return dtos;
    }

    public PenaltyDTO findPenaltyById(int id) {
        return penaltyWrapper.wrapToDTO(penaltyRepositroy.findOne(id));
    }

    public PenaltyDTO updatePenalty(int id, PenaltyDTO dto) {
        Penalty penalty = penaltyWrapper.wrapToEntity(dto);
        penalty.setId(id);

        return penaltyWrapper.wrapToDTO(penaltyRepositroy.save(penalty));
    }

    public void removePenalty(int id) {
        penaltyRepositroy.delete(id);
    }
}
